import unittest
import os

from knightboard import KnightBoard

DIR = os.path.dirname(os.path.abspath(__file__))


class TestLevelOne(unittest.TestCase):

    def test_from_string(self):
        with open(os.path.join(DIR, '../boards/blank_7x7.txt')) as fp:
            board_as_string = fp.read()
        kb = KnightBoard.from_string(board_as_string)
        self.assertEqual(kb.height, 7)
        self.assertEqual(kb.width, 7)

        with open(os.path.join(DIR, '../boards/big_32x32.txt')) as fp:
            board_as_string = fp.read()
        kb = KnightBoard.from_string(board_as_string)
        self.assertEqual(kb.height, 32)
        self.assertEqual(kb.width, 32)

    def test_is_valid_move(self):
        board = KnightBoard.from_txt_file(os.path.join(DIR, '../boards/blank_7x7.txt'))
        self.assertTrue(board.is_valid_move((0, 0), (2, 1)))
        self.assertTrue(board.is_valid_move((0, 0), (1, 2)))
        self.assertFalse(board.is_valid_move((0, 0), (1, 1)))
        self.assertFalse(board.is_valid_move((0, 0), (-1, 2)))
        self.assertFalse(board.is_valid_move((0, 0), (1, -2)))

        self.assertTrue(board.is_valid_move((6, 6), (4, 5)))
        self.assertTrue(board.is_valid_move((6, 6), (5, 4)))
        self.assertFalse(board.is_valid_move((6, 6), (4, 4)))
        self.assertFalse(board.is_valid_move((6, 6), (7, 8)))
        self.assertFalse(board.is_valid_move((6, 6), (8, 7)))

    def test_play_moves(self):
        kb = KnightBoard.from_txt_file(os.path.join(DIR, '../boards/blank_7x7.txt'))

        """
            S . . . . . .
            . . 1 . . . .
            . . . . 2 . .
            . . . . . . .
            . . . . . 3 .
            . . . . . . .
            . . . . . . E
        """
        moves = [(1, 2), (2, 4), (4, 5), (6, 6)]
        kb.set_position((0, 0))
        self.assertTrue(kb.play_moves(moves))

        """
            S E . . . . .
            . . 1 . . . .
            4 . . . . . .
            . . . 2 . . .
            . 3 . . . . .
            . . . . . . .
            . . . . . . .
        """
        moves = [(1, 2), (3, 3), (4, 1), (2, 0), (0, 1)]
        kb.set_position((0, 0))
        self.assertTrue(kb.play_moves(moves))

        """
            S . . . . . .
            . . 1 . . . .
            . . X . . . .
            . . . . . . .
            . . . . . . .
            . . . . . . .
            . . . . . . .
        """
        moves = [(1, 2), (2, 2), (4, 1), (2, 0), (0, 1)]
        kb.set_position((0, 0))
        self.assertFalse(kb.play_moves(moves))

        """
            S . . . . . .
            . . 1 . . . .
            . . . . . . .
            . 2 . . . . .
          X . . . . . . .
            . . . . . . .
            . . . . . . .
        """
        moves = [(1, 2), (3, 1), (-1, 5)]
        kb.set_position((0, 0))
        self.assertFalse(kb.play_moves(moves))
