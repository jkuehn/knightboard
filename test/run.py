import sys
import os
import logging
import unittest

DIR = os.path.dirname(os.path.abspath(__file__))


def main():
    test_suite = unittest.TestSuite()
    test_loader = unittest.TestLoader()

    pattern = 'test*.py'
    if sys.argv[1:]:
        pattern = sys.argv[1]
    test_suite.addTest(test_loader.discover(DIR, pattern=pattern))

    logging.basicConfig(
        format='%(asctime)-15s [%(levelname)-5s] %(name)s - %(message)s',
        datefmt="%Y-%m-%d %H:%M:%S%z",
        level=logging.INFO)
    test_runner = unittest.TextTestRunner(
        verbosity=1,
    )
    test_result = test_runner.run(test_suite)

    if test_result and not test_result.wasSuccessful():
        sys.exit(1)


if __name__ == '__main__':
    main()
