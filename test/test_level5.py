import unittest
import os

from knightboard import KnightBoard

DIR = os.path.dirname(os.path.abspath(__file__))


class TestLevelFive(unittest.TestCase):

    def test_longest_path_simple(self):
        # simple case, only one valid path
        board = ". .\n" \
                ". .\n" \
                ". ."
        kb = KnightBoard.from_string(board)
        kb.set_position((0, 0))
        path = kb.find_longest_path((2, 1))
        self.assertTrue(kb.play_moves(path))
        self.assertEqual(path, [(2, 1)])

        board = ". . .\n" \
                ". . .\n" \
                ". . ."
        kb = KnightBoard.from_string(board)
        kb.set_position((0, 0))
        path = kb.find_longest_path((2, 1))
        self.assertTrue(kb.play_moves(path))
        self.assertEqual(path, [(1, 2), (2, 0), (0, 1), (2, 2), (1, 0), (0, 2), (2, 1)])

    def test_bigboard_longest_path(self):
        kb = KnightBoard.from_txt_file(os.path.join(DIR, '../boards/big_32x32.txt'))
        kb.set_position((0, 0))
        path = kb.find_longest_path((31, 31))
        self.assertTrue(kb.play_moves(path))
        self.assertEqual(len(path), 418)

        # make sure there are no duplicates
        for i in range(len(path)):
            for j in range(i + 1, len(path)):
                self.assertNotEqual(path[i], path[j])

        kb.set_position((31, 0))
        path = kb.find_longest_path((0, 31))
        self.assertTrue(kb.play_moves(path))
        self.assertEqual(len(path), 509)

        # make sure there are no duplicates
        for i in range(len(path)):
            for j in range(i + 1, len(path)):
                self.assertNotEqual(path[i], path[j])



