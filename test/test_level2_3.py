import unittest
import os

from knightboard import KnightBoard

DIR = os.path.dirname(os.path.abspath(__file__))


class TestLevelTwoThree(unittest.TestCase):

    def test_find_shortest_path(self):
        kb = KnightBoard.from_txt_file(os.path.join(DIR, '../boards/blank_7x7.txt'))

        # invalid input
        try:
            kb.set_position((0, 0))
            moves = kb.find_shortest_path((-1, 0))
            self.assertTrue(False)
        except:
            pass

        # trivial case
        kb.set_position((0, 0))
        moves = kb.find_shortest_path((0, 0))
        self.assertEqual(len(moves), 0)

        # simple case (1 move only)
        kb.set_position((0, 0))
        moves = kb.find_shortest_path((2, 1))
        self.assertEqual(len(moves), 1)
        self.assertEqual(moves, [(2, 1)])

        # multiple moves
        kb.set_position((0, 0))
        moves = kb.find_shortest_path((6, 6))
        self.assertEqual(len(moves), 4)
        self.assertEqual(moves, [(2, 1), (4, 2), (5, 4), (6, 6)])
