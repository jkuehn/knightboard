import unittest
import os

from knightboard import KnightBoard

DIR = os.path.dirname(os.path.abspath(__file__))


class TestLevelFour(unittest.TestCase):

    def test_rocks_and_barriers(self):

        board = ". R R\n" \
                "R R R\n" \
                ". . ."
        kb = KnightBoard.from_string(board)
        self.assertTrue(kb.is_valid_move((0, 0), (2, 1)))
        self.assertFalse(kb.is_valid_move((0, 0), (1, 2)))

        board = ". B B .\n" \
                "B B B .\n" \
                ". . . .\n" \
                ". . . ."
        kb = KnightBoard.from_string(board)
        self.assertTrue(kb.is_valid_move((2, 0), (3, 2)))
        self.assertFalse(kb.is_valid_move((0, 0), (2, 1)))
        self.assertFalse(kb.is_valid_move((0, 0), (1, 2)))

    def test_no_solution(self):
        board = ". . . B B . . .\n" \
                ". . . B B . . .\n" \
                ". . . B B . . ."
        kb = KnightBoard.from_string(board)
        kb.set_position((0, 0))
        self.assertFalse(kb.find_shortest_path((2, 6)))

    def test_teleport_shortest_path(self):
        board = ". . . B B . T .\n" \
                ". . . B B . . .\n" \
                ". T . B B . . ."
        kb = KnightBoard.from_string(board)
        kb.set_position((0, 0))
        self.assertEqual(kb.find_shortest_path((2, 7)), [(2, 1), (2, 7)])
        self.assertTrue(kb.play_moves([(2, 1), (2, 7)]))

        board = ". . . . . . T .\n" \
                ". . . . . . . .\n" \
                ". T . . . . . ."
        kb = KnightBoard.from_string(board)
        kb.set_position((0, 0))
        self.assertEqual(kb.find_shortest_path((2, 7)), [(2, 1), (2, 7)])
        self.assertTrue(kb.play_moves([(2, 1), (2, 7)]))

    def test_water_and_lava_shortest_path(self):
        board = ". L . L . L\n" \
                ". L L L L L\n" \
                ". . L . L ."
        kb = KnightBoard.from_string(board)
        start, end = (0, 0), (2, 5)
        kb.set_position(start)
        self.assertEqual(kb.find_shortest_path(end), [(2, 1), (0, 2), (2, 3), (0, 4), (2, 5)])
        self.assertTrue(kb.play_moves([(2, 1), (0, 2), (2, 3), (0, 4), (2, 5)]))

        board = ". . W . . W W\n" \
                ". . . . . W W\n" \
                ". W W W . W W\n" \
                ". W W W . . ."
        kb = KnightBoard.from_string(board)
        start, end = (0, 0), (3, 6)
        kb.set_position(start)
        self.assertEqual(kb.find_shortest_path(end), [(1, 2), (2, 4), (3, 6)])

    def test_bigboard_shortest_path(self):
        kb = KnightBoard.from_txt_file(os.path.join(DIR, '../boards/big_32x32.txt'))
        kb.set_position((0, 0))
        path = kb.find_shortest_path((31, 31))
        self.assertTrue(kb.play_moves(path))
        self.assertTrue(len(path), 22)

        kb.set_position((31, 0))
        path = kb.find_shortest_path((0, 31))
        self.assertTrue(kb.play_moves(path))
        self.assertTrue(len(path), 22)



