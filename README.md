
# Knight Board


## Notes

- All board positions are in terms of (row, column) or (y, x).
- Check the commit history if you want to see my solution's progression.  I tried to make a commit after each level I finished.
- This was fun :)


### Level 1 - 3

Levels 1-3 were pretty straight forward.  I used a breadth first search to find the shortest path making sure to
keep track of nodes I already visited using a set.


### Level 4

Here is where things got a little trickier.  After adding checks for Barriers, Rocks, and Teleporters,
it became clear a normal breadth first wouldn't cut it due to the differing cost of traversing terrain.
At first I thought maybe an A* search would work well, however due to the way Knights move, coming up with a good
distance heuristic is challenging.


The solution I ended up with was to sort the leaf nodes with a minimum priority queue.  The cost function is the
distance (accounting for terrain costs) from the start.  In this way only the lowest cost paths are
explored first.


### Level 5

In order to find the longest path, I used a similar strategy as the shorter path but in stead of using
a minimum queue I used a maximum queue so that the most expensive paths are searched first. 


Also instead of stopping the search once the end position is first reached, I continue to search alternative 
paths until they have all been exhausted.  After all paths have been explored I select the one with the highest
cost and return it.


## To Run Tests:
```
python -m test.run
```