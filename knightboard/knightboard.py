from Queue import Queue
from priority_queue import PriorityQueue

EMPTY = '.'
KNIGHT = 'K'
WATER = 'W'
ROCK = 'R'
BARRIER = 'B'
TELEPORT = 'T'
LAVA = 'L'


class Node(object):
    pass


class KnightBoard:

    def __init__(self, board, start_position=None):
        if len(board) == 0:
            raise ValueError("board cannot be empty")
        self.board = board
        self.height = len(board)
        self.width = len(board[0])
        if start_position is None:
            self.current_position = (0, 0)
        else:
            self.set_position(start_position)

    valid_moves = [
        (2, 1), (2, -1),
        (-2, 1), (-2, -1),
        (1, 2), (1, -2),
        (-1, 2), (-1, -2),
    ]

    def is_valid_board_position(self, position):
        """
        Checks if the given position is valid within the board boundaries
        :param position: board location
        """
        return 0 <= position[0] < self.height and 0 <= position[1] < self.width and \
               self.get_terrain_at(position) not in [BARRIER, ROCK]

    def is_valid_move(self, cur_pos, next_pos):
        """
        Given the current position and the next position returns True if the move is valid
        taking into account the terrain
        :param cur_pos: current position
        :param next_pos: next position
        :return: True if the move is valid, False otherwise
        """
        if not self.is_valid_board_position(cur_pos):
            return False
        if not self.is_valid_board_position(next_pos):
            return False

        rel_pos = (next_pos[0] - cur_pos[0], next_pos[1] - cur_pos[1])
        if rel_pos not in self.valid_moves:
            return False

        # Check for barriers in the path that the knight would travel
        if rel_pos[0] == 2:
            path = [(1, 0), (2, 0)]
        elif rel_pos[0] == -2:
            path = [(-1, 0), (-2, 0)]
        elif rel_pos[1] == 2:
            path = [(0, 1), (0, 2)]
        else:
            path = [(0, -1), (0, -2)]
        for y, x in path:
            if self.get_terrain_at((cur_pos[0] + y, cur_pos[1] + x)) == BARRIER:
                return False

        return True

    def set_position(self, position):
        """
        Sets the current position of the knight to the specified location
        :param position: board location
        """
        if self.is_valid_board_position(position):
            self.current_position = position
        else:
            raise Exception("That's an impossible position dope!")

    def get_terrain_at(self, position):
        """
        Gets the cell of the given board position
        :param position: board position
        :return: the cell
        """
        return self.board[position[0]][position[1]]

    def get_moves_from_position(self, position):
        """
        Gets a list of valid moves from the specified position
        :param position: board location
        :return: list of moves
        """
        for y, x in self.valid_moves:
            move = (position[0] + y, position[1] + x)
            if self.is_valid_move(position, move):
                yield move

    def get_teleport_out(self, teleport_in):
        """
        Searches the board for the matching teleporter.
        :param teleport_in: position of one of the two teleporters
        :return: the postion of the output teleporter
        """
        if self.get_terrain_at(teleport_in) != TELEPORT:
            raise Exception("Cannot teleport from a non-teleport terrain")

        # find the other teleport on the map
        teleport_out = None
        for row in range(self.height):
            for col in range(self.width):
                pos = (row, col)
                if pos != teleport_in and self.get_terrain_at(pos) == TELEPORT:
                    teleport_out = pos
                    break

        if teleport_out is None:
            raise Exception("A second teleport was not found!")

        return teleport_out

    def play_moves(self, moves):
        """
        Attempts to play the provided moves.
        :param moves: list of moves
        :return: True if the moves are valid, False otherwise
        """
        if len(moves) == 0:
            return False

        # print self.__str__()
        for move in moves:
            if self.is_valid_move(self.current_position, move):
                if self.get_terrain_at(move) == TELEPORT:
                    teleport_out = self.get_teleport_out(move)
                    self.set_position(teleport_out)
                else:
                    self.set_position(move)
                # print self.__str__()
            else:
                return False
        return True

    def find_longest_path(self, end_position):
        """
        Find the longest non-repeating path to the end position
        :param end_position: board position to finish at
        :return: list of moves from the current position to the end position
        """
        if not self.is_valid_board_position(end_position):
            raise Exception("That's not a valid end position!")

        root = Node()
        root.pos = self.current_position
        root.parent = None
        root.cost = 0

        visited = set()
        visited.add(root.pos)

        queue = PriorityQueue()
        queue.push(root, root.cost)

        end_node = None
        while not queue.empty():
            node = queue.pop()
            if node.pos == end_position:
                if end_node is None or node.cost > end_node.cost:
                    end_node = node
                # instead of ending our search once we find the end node as we do
                # for the shortest path, keep exploring for longer paths
                continue
            else:
                visited.add(node.pos)

            # Find all the valid moves (child nodes) from the current location
            if self.get_terrain_at(node.pos) == TELEPORT:
                teleport_out = self.get_teleport_out(node.pos)
                moves = [m for m in self.get_moves_from_position(teleport_out) if m not in visited]
            else:
                moves = [m for m in self.get_moves_from_position(node.pos) if m not in visited]

            for m in moves:
                child = Node()
                child.parent = node
                child.pos = m
                # Add terrain costs
                terrain = self.get_terrain_at(m)
                if terrain == WATER:
                    child.cost = node.cost + 2
                elif terrain == LAVA:
                    child.cost = node.cost + 5
                else:
                    child.cost = node.cost + 1

                # Note that we assign the negative cost to invert the queue ordering
                queue.push(child, -child.cost)

        if end_node is None:
            return []  # No solution

        # Build the path by walking up the tree
        path = []
        cur = end_node
        while cur.parent is not None:
            path.append(cur.pos)
            cur = cur.parent
        path.reverse()
        return path

    def find_shortest_path(self, end_position):
        """
        Find the shortest non-repeating series of valid moves to the end position
        :param end_position: board position to finish at
        :return: list of moves from the current position to the end position
        """
        if not self.is_valid_board_position(end_position):
            raise Exception("That's not a valid end position!")

        root = Node()
        root.pos = self.current_position
        root.parent = None
        root.cost = 0

        visited = set()
        visited.add(root.pos)

        queue = PriorityQueue()
        queue.push(root, 0)

        # Perform the best-first search
        end_node = None
        while not queue.empty():
            node = queue.pop()
            if node.pos == end_position:
                end_node = node
                break

            # Find all the valid moves (child nodes) from the current location
            if self.get_terrain_at(node.pos) == TELEPORT:
                teleport_out = self.get_teleport_out(node.pos)
                moves = [m for m in self.get_moves_from_position(teleport_out) if m not in visited]
            else:
                moves = [m for m in self.get_moves_from_position(node.pos) if m not in visited]

            for m in moves:
                child = Node()
                child.parent = node
                child.pos = m
                # Add terrain costs
                terrain = self.get_terrain_at(m)
                if terrain == WATER:
                    child.cost = node.cost + 2
                elif terrain == LAVA:
                    child.cost = node.cost + 5
                else:
                    child.cost = node.cost + 1
                visited.add(m)
                queue.push(child, child.cost)

        if end_node is None:
            return []  # No solution

        # Build the path by walking up the tree
        path = []
        cur = end_node
        while cur.parent is not None:
            path.append(cur.pos)
            cur = cur.parent
        path.reverse()
        return path

    def __str__(self):
        ss = ''
        for row in range(self.height):
            for col in range(self.width):
                cell = self.board[row][col]
                if row == self.current_position[0] and col == self.current_position[1]:
                    ss += 'K '
                elif cell == EMPTY:
                    ss += '. '
                elif cell == WATER:
                    ss += 'W '
                elif cell == ROCK:
                    ss += 'R '
                elif cell == BARRIER:
                    ss += 'B '
                elif cell == LAVA:
                    ss += 'L '
                elif cell == TELEPORT:
                    ss += 'T '
                else:
                    raise Exception("That's an invalid cell type ya dummy!")
            ss += '\n'
        return ss

    @classmethod
    def from_string(cls, board_as_string):
        if not board_as_string:
            raise ValueError("board_as_string is invalid")
        rows = board_as_string.split('\n')
        board = []
        for row in rows:
            board.append(row.split(' '))
        return cls(board)

    @classmethod
    def from_txt_file(cls, filename):
        with open(filename, 'r') as fp:
            return cls.from_string(fp.read())
