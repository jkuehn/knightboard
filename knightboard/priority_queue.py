import heapq


class PriorityQueue:
    """
    Simple priority queue implementation modified from Python Cookbook 3rd Edition
    https://www.safaribooksonline.com/library/view/python-cookbook-3rd/9781449357337/ch01s05.html
    """
    def __init__(self):
        self._queue = []
        self._index = 0

    def push(self, item, priority):
        heapq.heappush(self._queue, (priority, self._index, item))
        self._index += 1

    def pop(self):
        return heapq.heappop(self._queue)[-1]

    def empty(self):
        return len(self._queue) == 0
